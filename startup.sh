#!/bin/bash

filename=url.txt
binfile=bin/debug/testbin
resultpath=result
result=result_file

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:$(pwd)/bin/debug

mkdir -p $(pwd)/$resultpath 2>&1 >>/dev/null
ulimit -c unlimited

count=0;
index=1;
cat "$filename" | while read line
do
    let "count = $count + 1"

    val=$(($count%2000));
    if (( $val == 0 )) ;then
	let "index = $index + 1"
    fi

    echo "[ $count] Handle URL:$line"
    echo "###################################" >> "$resultpath/$result.$index.txt"
    echo "URL:$line" >> "$resultpath/$result.$index.txt"
    echo "Number: $count" >> "$resultpath/$result.$index.txt"
    echo "---------------------------------------------" >> "$resultpath/$result.$index.txt"
    echo "Library:NPCE" >> "$resultpath/$result.$index.txt"
    $PWD/$binfile $line >> "$resultpath/$result.$index.txt"
    echo "---------------------------------------------" >> "$resultpath/$result.$index.txt"
    echo "Library:CONSOR" >> "$resultpath/$result.$index.txt"
    $PWD/consor/run_consor.sh $line >> "$resultpath/$result.$index.txt"
done

echo "Finished all"
